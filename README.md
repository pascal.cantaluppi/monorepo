# Monorepo

Extensible Dev Tools for Monorepos - Prototype

## Nx.dev

<p align="center">
  <img src="https://gitlab.com/pascal.cantaluppi/monorepo/-/raw/master/img/Nx.png" width="150" alt="Nx" /></a>
</p>

`Nx is a set of extensible dev tools for monorepos, which helps you develop like Google, Facebook, and Microsoft.`

Nx helps scale your development from one team building one application to many teams building multiple frontend and backend applications all in the same workspace. When using Nx, developers have a holistic dev experience powered by an advanced CLI (with editor plugins), capabilities for controlled code sharing and consistent code generation.

<p align="center">
  <img src="https://gitlab.com/pascal.cantaluppi/monorepo/-/raw/master/img/mono.png" width=200" alt="mono" /></a>
</p>

